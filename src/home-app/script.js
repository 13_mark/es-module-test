const Config = [
    {
        "url": "http://localhost:3333/bundle.js"
    }
];


Config.forEach(
    (element) => {
        const head = document.getElementsByTagName('head')[0];
        const script = document.createElement('script');
        script.setAttribute('type', 'module');
        script.setAttribute('src', element.url);
        head.appendChild(script);

        script.onload = async () => {
            const config = await import(element.url);
            const container = document.getElementById('custom-element-container');
            const customElement = document.createElement(config.config.name);
            container.appendChild(customElement)
        }
        script.onerror = () => console.log('error occured');
    }
)
