const ELEMENT_NAME = 'first-el';

const config = {
    name: ELEMENT_NAME,
    availableSizes: [
        { spanX: 3, spanY: 4 },
        { spanX: 2, spanY: 1 },
        { spanX: 2, spanY: 2 }
    ]
};

class FirstElement extends HTMLElement {
    connectedCallback() {
        this.innerHTML = "<b>Here is the first element.</b>";
    }

}
window.customElements.define(ELEMENT_NAME, FirstElement);


export { config };